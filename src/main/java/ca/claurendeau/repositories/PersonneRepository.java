package ca.claurendeau.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ca.claurendeau.domaine.Personne;

public interface PersonneRepository extends JpaRepository<Personne, Long>{

    Personne findByNom(String nom);

}
