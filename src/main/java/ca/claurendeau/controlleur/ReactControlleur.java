package ca.claurendeau.controlleur;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import ca.claurendeau.domaine.Message;
import ca.claurendeau.domaine.Personne;
import ca.claurendeau.repositories.PersonneRepository;

@RestController
public class ReactControlleur {
    
    @Autowired
    private PersonneRepository personeRepository;
    
    @CrossOrigin
    @GetMapping("/api/handlemessage")
    public Message handleMessage() {
        return new Message("Cherche un nom");
    }
    
    @CrossOrigin    
    @GetMapping("/api/trouvepersonne/{nom}")
    public Personne trouvePersonne(@PathVariable String nom) {
        return personeRepository.findByNom(nom);
    }
}
